# Example Setup of a Node.js Project

This is an opinionated setup of a Node.js project using Typescript. The idea
is that this repository can server as a starting place to get configuration to
setup new projects.

## Linting

Linting is setup using [eslint](https://eslint.org/) and my own customized
take on [Standard JS](https://standardjs.com/). The major differences between
my style and Standard JS are the use of semicolons and bracket placement.
You can find the full cusotmization at [eslint-config-bhdouglass](https://gitlab.com/bhdouglass/eslint-config-bhdouglass/-/blob/master/index.js).

## Formatting

Formatting is done via a pretty standard [Prettier](https://prettier.io/)
configuration. This setup matches with the linter to avoid them fighting back
and forth.

[Husky](https://typicode.github.io/husky/#/) is setup so the formatter and linter
run before each commit.

There is also an [`.editorconfig`](https://editorconfig.org/) file setup to match.

## License

Copyright (C) 2023 [Brian Douglass](http://bhdouglass.com/)

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
